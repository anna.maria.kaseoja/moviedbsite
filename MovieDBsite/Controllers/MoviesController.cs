﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MovieDBsite.Models;

namespace MovieDBsite.Controllers
{
    public class MoviesController : Controller
    {
        private MovieEntities db = new MovieEntities();

        // GET: Movies
        public ActionResult Index()
        {
            var movies1 = db.Movies1.Include(m => m.MovieCategory);
            return View(movies1.ToList());
        }

     
        // SEARCH
        [HttpPost]
        public ActionResult Index(FormCollection coll)
        {
            bool comedy = (coll["CheckboxComedy"]??"") == "on";
            bool ScyFy = (coll["CheckboxScyFy"] ?? "") == "on";
            bool Cartoon = (coll["CheckboxCartoon"] ?? "") == "on";
            bool Horror = (coll["CheckboxHorror"] ?? "") == "on";
            bool Fantasy = (coll["CheckboxFantasy"] ?? "") == "on";


            var movies1 = db.Movies1.Include(m => m.MovieCategory).ToList();

            if (comedy) movies1 = db.MovieCategories.Where(x => x.Name == "Comedy").Single().Movies.ToList();
            if (ScyFy) movies1 = db.MovieCategories.Where(x => x.Name == "Science fiction").Single().Movies.ToList();
            if (Cartoon) movies1 = db.MovieCategories.Where(x => x.Name == "Cartoon").Single().Movies.ToList();
            if (Horror) movies1 = db.MovieCategories.Where(x => x.Name == "Horror").Single().Movies.ToList();
            if (Fantasy) movies1 = db.MovieCategories.Where(x => x.Name == "Fantasy").Single().Movies.ToList();

            if (coll["Text1"] != null) movies1 = movies1.Where(x => x.Title.ToLower().Contains(coll["Text1"].ToLower())).ToList();
            return View(movies1.ToList());
        }

        // GET: Movies/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Movie movie = db.Movies1.Find(id);
            if (movie == null)
            {
                return HttpNotFound();
            }
            return View(movie);
        }

        // GET: Movies/Create
        public ActionResult Create()
        {
            ViewBag.CategoryId = new SelectList(db.MovieCategories, "Id", "Name");
            return View();
        }

        // POST: Movies/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Title,Year,Description,Rating,CategoryId")] Movie movie)
        {
            if (ModelState.IsValid)
            {
                db.Movies1.Add(movie);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CategoryId = new SelectList(db.MovieCategories, "Id", "Name", movie.CategoryId);
            return View(movie);
        }

        // GET: Movies/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Movie movie = db.Movies1.Find(id);
            if (movie == null)
            {
                return HttpNotFound();
            }
            ViewBag.CategoryId = new SelectList(db.MovieCategories, "Id", "Name", movie.CategoryId);
            return View(movie);
        }

        // POST: Movies/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Title,Year,Description,Rating,CategoryId")] Movie movie)
        {
            if (ModelState.IsValid)
            {
                db.Entry(movie).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CategoryId = new SelectList(db.MovieCategories, "Id", "Name", movie.CategoryId);
            return View(movie);
        }

        // GET: Movies/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Movie movie = db.Movies1.Find(id);
            if (movie == null)
            {
                return HttpNotFound();
            }
            return View(movie);
        }

        // POST: Movies/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Movie movie = db.Movies1.Find(id);
            db.Movies1.Remove(movie);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
