/*
Movie [id (int), title, year (int), description, rating (int), category id], Movies list and Category (id, name)
*/
create table MovieCategories
(
	Id int identity primary key,
	Name nvarchar(32),
	Description nvarchar(max)
)

create table Movies
(
	Id int identity primary key,
	Title nvarchar(100),
	[Year] int,
	Description nvarchar(max),
	Rating int,
	CategoryId int references MovieCategories(id)
)